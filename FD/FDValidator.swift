//
//  FDValidator.swift
//  WebMii
//
//  Created by Florent Douine on 27/10/2015.
//  Copyright © 2015 Florent Douine. All rights reserved.
//

import Foundation
import UIKit

public protocol FDValidity{
  func isValid(_ value: String?)->Bool
  func translatedMessage(_ name: String, value: String?)->String
}

public enum FDNSStringValidity: FDValidity{
  case notEmpty
  case alphanumeric
  case containSpaces(Int)
  case name
  case lessThan(Int)
  case greaterThan(Int)
  case email
  
  public func isValid(_ value: String?) -> Bool{
    
    guard let v = value else{
      return false
    }
    
    switch self {
    case .notEmpty :
      return v.count>0
      
    case .containSpaces(let numberOfSpaces) :
      return v.components(separatedBy: " ").count-1 >= numberOfSpaces
      
    case .name :
      let nameRegex: String = "[\\p{Letter} '-]+"
      let nameTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", nameRegex)
      return (nameTest.evaluate(with: v))
      
    case .alphanumeric :
      let alphaRegex: String = "[A-Z0-9a-z]+"
      let alphaTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", alphaRegex)
      return (alphaTest.evaluate(with: v))
      
    case .lessThan(let numberOfCharacters) :
      return v.count < numberOfCharacters
      
    case .greaterThan(let numberOfCharacters) :
      return v.count > numberOfCharacters
      
    case .email :
      let emailRegex: String = "[A-Z0-9a-z.%+-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
      let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
      return (emailTest.evaluate(with: v))
    }
    
  }
  
  
  public func translatedMessage(_ name: String, value: String?)->String{
    
    
    switch self{
    case .notEmpty :
      return String(format: FDLocalized("error.required"), name)
    case .alphanumeric:
      return String(format: FDLocalized("error.invalid_alpha"), name)
    case .containSpaces(let numberOfCharacters):
      return String(format: FDLocalized("error.contain_spaces"), name, numberOfCharacters)
    case .name:
      return String(format: FDLocalized("error.incorrect_name"), name)
    case .lessThan(let numberOfCharacters):
      return String(format: FDLocalized("error.less_than"), name, numberOfCharacters-1)
    case .greaterThan(let numberOfCharacters):
      return String(format: FDLocalized("error.greater_than"), name, numberOfCharacters+1)
    case .email:
      return String(format: FDLocalized("error.invalid_email"), name)
    }
    
    
  }
  
  
}

open class FDField {
  
  open var required: Bool = true
  
  var name: String
  
  var value: String?
  
  var validities: [FDValidity]
  
  var overrideErrorMessage: String?
  
  var errorMessage: String{
    
    if let message = overrideErrorMessage{
      return message
    }
    
    var translatedMessage: [String] = []
    validities.forEach({ (validity) -> () in
      if(!validity.isValid(self.value)){
        translatedMessage.append(validity.translatedMessage(self.name, value: self.value))
      }
    })
    
    return translatedMessage.joined(separator: "\n")
  }
  
  open var isValid: Bool {
    
    if((value ?? "").isEmpty  && !required){
      return true
    }
    
    for validity in self.validities{
      if(!validity.isValid(value)){
        return false
      }
    }
    return true
  }
  
  public init(displayName: String, andValue value: String?, andValidity stringValidity: FDValidity...) {
    self.name = displayName
    self.value = value
    self.validities = stringValidity
    
  }
    
    public init(displayName: String, andValue value: String?, andValidities stringValidities: [FDValidity]) {
        self.name = displayName
        self.value = value
        self.validities = stringValidities
        
    }
}

open class FDValidator {
  var fields: [FDField]? = []
  
  var returnOnFirstError = false
  
  public init(fields: [FDField]){
    self.fields = fields
  }
  
  
  open func checkValidity() -> Bool {
    
    guard let _fields = fields else{
      return true
    }
    
    for field: FDField in _fields {
      if(!field.isValid){
        return false
      }
    }
    return true
  }
  
  
  open func checkValidity(success:()->(), failed: (String, UIAlertController, [FDField])->()) {
    
    guard let _fields = fields else{
      success()
      return
    }
    
    var fieldsInError: [FDField] = []
    
    var translatedMessage: [String] = []
    
    for field: FDField in _fields {
      if(!field.isValid){
        fieldsInError.append(field)
        translatedMessage.append(field.errorMessage)
      }
    }
    
    if(fieldsInError.isEmpty){
      success()
      return
    }
    
    
    var message = translatedMessage.joined(separator: "\n")
    if(returnOnFirstError){
      message = message.components(separatedBy: "\n")[0]
    }
    
    //let title = localized("global_error") ?? FDLocalized("global.error")
    let alertController = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
    alertController.addAction(UIAlertAction(title: FDLocalized("global.ok"), style: UIAlertAction.Style.default, handler: nil))
    
    failed(message, alertController, fieldsInError);
    
  }
  
  
  
  
  
}
