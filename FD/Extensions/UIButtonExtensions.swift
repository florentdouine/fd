//
//  UIButtonExtensions.swift
//  SwifterSwift
//
//  Created by Omar Albeik on 8/22/16.
//  Copyright © 2016 Omar Albeik. All rights reserved.
//

import UIKit

public extension UIButton {
	/// Set image for all states.
    func setImageForAllStates(_ image: UIImage?) {
		setImage(image, for: .normal)
		setImage(image, for: .selected)
		setImage(image, for: .highlighted)
		setImage(image, for: .disabled)
	}
	
	/// Set title color for all states.
    func setTitleColorForAllStates(_ color: UIColor?) {
		setTitleColor(color, for: .normal)
		setTitleColor(color, for: .selected)
		setTitleColor(color, for: .highlighted)
		setTitleColor(color, for: .disabled)
	}
	
	
	/// Set title for all states.
    func setTitleForAllStates(_ title: String?) {
		setTitle(title, for: .normal)
		setTitle(title, for: .selected)
		setTitle(title, for: .highlighted)
		setTitle(title, for: .disabled)
	}
   
    func setBackgroundImageForAllStates(_ image: UIImage?){
        setBackgroundImage(image, for: .normal)
        setBackgroundImage(image, for: .selected)
        setBackgroundImage(image, for: .highlighted)
        setBackgroundImage(image, for: .disabled)
    }
}
