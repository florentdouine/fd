//
//  UINavigationControllerExtension.swift
//  PwC
//
//  Created by Florent Douine on 15/11/2016.
//  Copyright © 2016 Florent Douine. All rights reserved.
//

import Foundation

public extension UINavigationController {
    class var main: UINavigationController?{
        return UIApplication.shared.windows.first?.rootViewController as? UINavigationController
    }
}
