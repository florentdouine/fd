//
//  IntExtension.swift
//  PwC
//
//  Created by Florent Douine on 09/11/2016.
//  Copyright © 2016 Florent Douine. All rights reserved.
//

import Foundation

public extension Int {
    func formatThousand() -> String {
        let fmt = NumberFormatter()
        fmt.numberStyle = .decimal
        return fmt.string(from: NSNumber(integerLiteral: self))!  // with my locale, "2,358,000"
    }
    
    mutating func roundToNextDecade() -> Double {
        var decade : Double = 1
        while self >= 10 {
            decade += 1
            self = self/10
        }
        
        //Arrondi à la dizaine supérieur
        return pow(10.0, decade)
        
        //Arrondi à l'entier supérieur
        //return Double(self+1) * pow(10.0, decade)
        
    }
}
