//
//  UILabelExtension.swift
//  Enedis
//
//  Created by Florent Douine on 13/09/2016.
//  Copyright © 2016 Florent Douine. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel {
    func setText(_ title: String, titleFont: UIFont, titleColor: UIColor, subtitle: String?, subtitleFont: UIFont?, subtitleColor: UIColor?) {
        
        let mutableTitleString = NSMutableAttributedString(string: title,
                                                           attributes: [NSAttributedString.Key.font: titleFont, NSAttributedString.Key.foregroundColor: titleColor])
        
        let attributedTitle = NSMutableAttributedString(attributedString: mutableTitleString)
        if let _ = subtitle{
            let mutableSubtitleString = NSMutableAttributedString(string: "\n"+subtitle!, attributes: [NSAttributedString.Key.font: subtitleFont ?? titleFont, NSAttributedString.Key.foregroundColor: subtitleColor ?? titleColor])
            attributedTitle.append(mutableSubtitleString)
        }
        
        self.attributedText = attributedTitle
    }
}
