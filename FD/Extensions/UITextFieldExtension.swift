//
//  UIExtension.swift
//  WebMii
//
//  Created by Florent Douine on 29/10/2015.
//  Copyright © 2015 Florent Douine. All rights reserved.
//

import Foundation
import UIKit

let UIKeyboardDoneButtonActivated = "UIKeyboardDoneButtonActivated";
public extension UITextField{
  
    var grayPlaceholder: Bool{
    set(key){
      attributedPlaceholder = NSAttributedString(string:placeholder!, attributes:[NSAttributedString.Key.foregroundColor: UIColor.lightGray]);
    }
    get{
      return attributedPlaceholder != nil;
    }
  }
  
    var withDoneBarButton: Bool {
    set (key) {
      
      let toolBar = UIToolbar()
      toolBar.tintColor = UIColor.lightGray
        
      let doneButton = UIBarButtonItem(title: FDLocalized("global.ok"), style: .plain, target: self, action: #selector(UITextField.doneButtonActivated))
      let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
      
      toolBar.sizeToFit()
      toolBar.setItems([space, doneButton], animated: true)
      inputAccessoryView = key ? toolBar : nil
    }
    get {
      return inputAccessoryView != nil
    }
  }
  
    @objc func doneButtonActivated(){
    resignFirstResponder()
    NotificationCenter.default.post(name: Notification.Name(rawValue: UIKeyboardDoneButtonActivated), object: self);
    
  }
}
