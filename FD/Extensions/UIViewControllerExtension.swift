//
//  FDHelpers.swift
//  Infogerant
//
//  Created by Florent Douine on 13/05/2016.
//  Copyright © 2016 Florent Douine. All rights reserved.
//

import Foundation
import UIKit

public typealias BasicCompletion = () -> (Void)

public extension UIViewController {
    
    func show(_ message: String?){
        self.show(message ?? "Erreur inconnue", title: nil, completion: nil)
    }
    
    func show(_ message: String, title:String?, completion: (() -> Void)? ){
        let alertController = UIAlertController(title: title ?? "Erreur", message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
            completion?()
        }))
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    func show(_ message: String, title:String?, buttons: [(title: String, style: UIAlertAction.Style, completion: BasicCompletion?)] ){
        let alertController = UIAlertController(title: title ?? "Erreur", message: message, preferredStyle: UIAlertController.Style.alert)
        
        for button in buttons {
            alertController.addAction(UIAlertAction(title: button.title, style: button.style, handler: { (alertAction) in
                button.completion?()
            }))
        }
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    func confirm(_ message: String, title:String?, yes: @escaping (() -> Void), no: (() -> Void)?){
        let alertController = UIAlertController(title: title ?? "Confirmation", message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Oui", style: .default, handler: { (alertAction) in
            yes()
        }))
        alertController.addAction(UIAlertAction(title: "Non", style: .cancel, handler: { (alertAction) in
            no?()
        }))
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    static func getVisibleViewController(_ rootVC: UIViewController?) -> UIViewController? {
        
        let unknownVC = rootVC ?? UIApplication.shared.keyWindow?.rootViewController
        
        if let presenting = unknownVC?.presentedViewController {
            return getVisibleViewController(presenting)
        }
        
        if let navigationController = unknownVC as? UINavigationController{
            return getVisibleViewController(navigationController.visibleViewController)
        }
        
        if let tabBarController = unknownVC as? UITabBarController {
            return getVisibleViewController(tabBarController.selectedViewController)
        }
        
        
        if let splitViewController = unknownVC as? UISplitViewController {
            return getVisibleViewController(splitViewController.viewControllers[1])
        }
        
        return unknownVC
    
    }
}

public extension UIViewController
{
    class func instantiateFromStoryboard() -> Self
    {
        return instantiateFromStoryboardHelper(self, storyboardName: "Main")
    }
    
    class func instantiateFromStoryboard(storyboardName: String) -> Self
    {
        return instantiateFromStoryboardHelper(self, storyboardName: storyboardName)
    }
    
    private class func instantiateFromStoryboardHelper<T>(_ selftype: T.Type, storyboardName: String) -> T
    {
        
        let storyboardId = String(describing: selftype)
        let storyboad = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = storyboad.instantiateViewController(withIdentifier: storyboardId) as! T
        
        return controller
    }
}



