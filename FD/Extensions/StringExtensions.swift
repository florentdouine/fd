//
//  StringExtensions.swift
//  Pods
//
//  Created by Florent Douine on 01/02/2017.
//
//

import UIKit

public extension Optional {

    public func or(_ defaultString: String) -> String {
        switch self {
        case .some(let value):
            let s = String(describing: value)
            return s.isEmpty ? defaultString : s
        case _:
            return defaultString
        }
    }
    
}
