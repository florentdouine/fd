//
//  FDConstants.swift
//  WebMii
//
//  Created by Florent Douine on 27/10/2015.
//  Copyright © 2015 Florent Douine. All rights reserved.
//

import Foundation


public func FDLocalized(_ s: String?)->String{
    let podBundle = Bundle(for: FDField.self)
    return NSLocalizedString(s ?? "", tableName: "FDLocalizable", bundle: podBundle, value: s ?? "", comment: "")
}



